---
# gitlab-yaml-shellcheck: main=../cki_pipeline.yml

.common-export-vars: |
  # Export variables that we need to build kcidb schema
  KCIDB_CHECKOUT_ID="$(create_checkout_id "")"
  # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
  if is_debug_build; then
    BUILD_PACKAGE_NAME="${package_name%-debug}-debug"
  else
    BUILD_PACKAGE_NAME="${package_name}"
  fi
  KCIDB_BUILD_ID="${KCIDB_CHECKOUT_ID}-${ARCH_CONFIG:-}-${BUILD_PACKAGE_NAME}"
  KCIDB_TEST_PREFIX=upt
  if [[ ${test_runner} != beaker ]] ; then
      KCIDB_TEST_PREFIX=${KCIDB_TEST_PREFIX}_${test_runner}
  fi
  export KCIDB_CHECKOUT_ID KCIDB_BUILD_ID KCIDB_TEST_PREFIX

.before-setup: |
    # Set up the path for locally installed python executables and ccache.
    # NOTE(mhayden): This cannot be added in a pipeline environment variable
    # because it disrupts the paths that the gitlab-runner expects in the
    # container when it starts.
    # create the ccache directory as well as otherwise ccache on RHEL6 is unhappy
    if is_true "${CCACHE_ENABLE}"; then
      export PATH="/usr/lib64/ccache:${PATH}"
      mkdir -p "${CCACHE_DIR}"
    fi
    export PATH="${HOME}/.local/bin:${PATH}"

    # Apply OpenShift UID/GID workarounds.
    if [ -w '/etc/passwd' ]; then
      echo "cki:x:$(id -u):$(id -g):,,,:${HOME}:/bin/bash" >> /etc/passwd
    fi
    if [ -w '/etc/group' ]; then
      echo "cki:x:$(id -G | cut -d' ' -f 2)" >> /etc/group
    fi

    # Set up basic configuration items for git.
    git config --global user.name "CKI Pipeline"
    git config --global user.email "cki-project@gitlab.com"
    git config --global advice.detachedHead false
    git config --global rerere.enabled false

    # Ensure home directory is set.
    if [ -z "${HOME:-}" ]; then
      export HOME=/tmp
    fi

    # Configure retries for wget
    cat >~/.wgetrc <<EOF
       tries = ${MAX_TRIES}
       # maximum wait time used for linear backoff
       waitretry = ${MAX_WAIT}
    EOF

    # Configure constraints file for pip
    echo "${CONSTRAINTS}" > "${CI_PROJECT_DIR}/constraints.txt"

    # Ensure the workdir and artifacts directory exist
    mkdir -p "${WORKDIR}" "${ARTIFACTS_DIR}"

    # Get CPU count related variables for compiling kernels, running xz etc.
    eval "$(get_cpu_count)"

    # Retrieve artifacts from S3
    aws_s3_download_artifacts

    # software tarball is not available in prepare stage
    if [ "${CI_JOB_STAGE}" == 'prepare' ]; then
      setup_software
    else
      extract_software
      # smoke test with the reporter to catch cases where the venv was prepared
      # for an outdated Python version, and fall back to direct installation
      if ! "${VENV_PY3}" -m reporter --help > /dev/null 2>&1; then
        setup_software
      fi
    fi

.before-info: |
    # Display information about the pipeline environment
    echo "🏠 Home directory: ${HOME}"

    if [[ -r /etc/cki-image ]]; then
      echo "📦 Container image:"
      sed 's/^/  /' /etc/cki-image
    fi

    if [ -v CI_PIPELINE_ID ]; then
      echo "🏗️ API URLs:"
      echo "  Pipeline: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}"
      echo "  Variables: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/variables"
      echo "  Job: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}"
    fi

    echo "💻 CPU and job count:"
    echo "  CPU count: ${CPUS_AVAILABLE}"
    echo "  Job count: ${MAKE_JOBS}"

.after-helpful-links: |
  # Display a couple of relevant links
  echo "This pipeline uses artifacts stored on S3:"
  cki_echo_notify " $(browsable_artifact_directory_url '')"
  if is_production ; then
      echo "More information about this pipeline can be found in DataWarehouse:"
    if [ -v ARCH_CONFIG ] ; then
      cki_echo_notify "  https://datawarehouse.cki-project.org/kcidb/builds/${KCIDB_BUILD_ID}"
    else
      cki_echo_notify "  https://datawarehouse.cki-project.org/kcidb/checkouts/${KCIDB_CHECKOUT_ID}"
    fi
  fi
  if [ -n "${mr_id}" ] ; then
    echo "Check out CKI documentation if you have questions about next steps:"
    cki_echo_notify "  https://cki-project.org/l/devel-faq/"
  fi

.common-before-script:
  - |
    #
    echo -e "\e[0Ksection_start:$(date +%s):before_script[collapsed=true]\r\e[0K\e[36;1mExport needed functions and variables (click to expand)...\e[0m"
  - set -euo pipefail
  - !reference [.general-function-definitions]
  - !reference [.aws-function-definitions]
  - !reference [.kcidb-function-definitions]
  - !reference [.prepare-function-definitions]
  - !reference [.common-export-vars]
  - !reference [.before-setup]
  - echo -e "\e[0Ksection_end:$(date +%s):before_script\r\e[0K"
  - |
    #
    echo -e "\e[0Ksection_start:$(date +%s):before_info[collapsed=true]\r\e[0K\e[36;1mGathering information about the pipeline environment (click to expand)...\e[0m"
  - !reference [.before-info]
  - echo -e "\e[0Ksection_end:$(date +%s):before_info\r\e[0K"

.common-after-script-head:
  - |
    #
    echo -e "\e[0Ksection_start:$(date +%s):after_script_head[collapsed=true]\r\e[0K\e[36;1mExport needed functions and variables (click to expand)...\e[0m"
  - !reference [.general-function-definitions]
  - !reference [.aws-function-definitions]
  - !reference [.kcidb-function-definitions]
  - !reference [.common-export-vars]
  - echo -e "\e[0Ksection_end:$(date +%s):after_script_head\r\e[0K"

.common-after-script-tail:
  - |
    #
    echo -e "\e[0Ksection_start:$(date +%s):after_script_tail[collapsed=true]\r\e[0K\e[36;1mUploading artifacts to AWS (click to expand)...\e[0m"
  - aws_s3_upload_artifacts
  - datawarehouse_upload_kcidb
  - echo -e "\e[0Ksection_end:$(date +%s):after_script_tail\r\e[0K"
  - !reference [.after-helpful-links]
