#!/bin/bash
# disable warnings about masked return values as this use is very common in unit tests
# shellcheck disable=SC2312
# shellcheck disable=SC2317  # disable warnings about unreachable mocking code

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

_failed_init

function check_owner_repo {
    declare -A OWNER_REPOS

    OWNER_REPOS[http://git-extensions-are-stripped/a/b/c.git]=b.c
    OWNER_REPOS[http://extension-and-slashes-are-stripped/a/b/c.git/]=b.c
    OWNER_REPOS[http://slashes-are-stripped/a/b/c/]=b.c
    OWNER_REPOS[http://two-directories/a/b/c]=b.c
    OWNER_REPOS[http://one-directory/a/b]=a.b
    OWNER_REPOS[http://no-directory/a]=git.a
    OWNER_REPOS[http://UPPER-CASE/A/B/C]=b.c

    OWNER_REPOS[git://host.com/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[http://host.com/git/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[git://host.com/name2.git]=git.name2
    OWNER_REPOS[http://host.com/git/name2.git]=git.name2
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git]=arm64.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git]=davem.net-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/jejb/scsi.git]=jejb.scsi
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git]=next.linux-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux.git]=powerpc.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git]=rdma.rdma
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git]=rt.linux-rt-devel
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/sashal/linux-stable.git]=sashal.linux-stable
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git]=stable.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git]=stable.linux-stable-rc
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/stable-queue.git]=stable.stable-queue
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git]=torvalds.linux
    OWNER_REPOS[https://gitlab.com/CKI-project/kernel-ark.git]=cki-project.kernel-ark
    OWNER_REPOS[https://host.com/gerrit/kernel-name3]=gerrit.kernel-name3
    OWNER_REPOS[https://host.com/gerrit/kernel-name4.git]=gerrit.kernel-name4

    for URL in "${!OWNER_REPOS[@]}"; do
        _check_equal "$(get_owner_repo "${URL}")" "${OWNER_REPOS[${URL}]}" "owner.repo" "Is the owner.repo correct for ${URL}"
    done
}
check_owner_repo

# function() instead of function{} so that the stub functions are scoped
function check_git_cache_clone()
(
    declare aws_s3_download_params
    function git {
        /usr/bin/git "$@" > /dev/null 2>&1
        echo "git_params='$*'" >&2
    }
    function aws_s3_download {
        mkdir -p test-output/git-repo
        /usr/bin/git init test-output/git-repo > /dev/null 2>&1
        tar -C test-output/git-repo -cf "${GIT_CACHE_DIR}/${FILE}" .
        echo "aws_s3_download_params='$*'" >&2
    }
    trap 'rm -rf test-output/ tests/workdir/' EXIT
    local URL="$1"
    local OWNER="$2"
    local FILE="$3"
    local git_params=''
    export GIT_CACHE_DIR="test-output/git-cache"
    mkdir -p "${GIT_CACHE_DIR}"
    eval "$(GIT_URL_OWNER=${OWNER} git_cache_clone "${URL}" tests/workdir --quiet 2>&1 >/dev/null)"
    read -ra git_params_array <<< "${git_params}"
    read -ra aws_s3_download_params_array <<< "${aws_s3_download_params}"

    _check_equal "${aws_s3_download_params_array[0]}" "BUCKET_GIT_CACHE" "bucket" "Does git_cache_clone pass the bucket correctly into aws_s3_download"
    _check_equal "${aws_s3_download_params_array[1]}" "${FILE}" "tarfile" "Does git_cache_clone pass the tar file name correctly into aws_s3_download"
    _check_equal "${git_params_array[-1]}" "--quiet" "param" "Does git_cache_clone pass the additional parameters to git clone"
    _check_equal "$(stat test-output/git-repo/.git/config > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"
)
check_git_cache_clone https://host.com/a/b/repo.git "" b.repo.tar
check_git_cache_clone https://host.com/a/b/repo.git "kernel" kernel.repo.tar

# function() instead of function{} so that the stub functions are scoped
function check_git_clone()
(
    function git {
        :
    }
    _check_equal "$(git_clone "$1" /)" "$2" "output" "Are the credentials correctly hidden for $1"
)
check_git_clone https://oauth2:secret-token@gitlab.example/a/b/repo.git "[1;32mCloning https://REDACTED@gitlab.example/a/b/repo.git/ from upstream[0m"

# function() instead of function{} so that the stub functions are scoped
function check_cpu_count()
(
    local NPROC_RESULT="$1"
    local CGROUPS_V1_QUOTA_RESULT="$2"
    local CGROUPS_V2_QUOTA_RESULT="$3"
    local EXPECTED_CPUS_AVAILABLE="$4"
    local EXPECTED_MAKE_JOBS="$5"
    local EXPECTED_RPM_BUILD_NCPUS="$6"
    local MESSAGE="$7"
    function nproc {
        echo "${NPROC_RESULT}"
    }
    function get_cgroups_v1_quota {
        echo "${CGROUPS_V1_QUOTA_RESULT}"
    }
    function get_cgroups_v2_quota {
        echo "${CGROUPS_V2_QUOTA_RESULT}"
    }
    eval "$(get_cpu_count)"

    # shellcheck disable=SC2154
    _check_equal "${CPUS_AVAILABLE}" "${EXPECTED_CPUS_AVAILABLE}" cpu_count "Is the CPU count correct with ${MESSAGE}"
    # shellcheck disable=SC2154
    _check_equal "${MAKE_JOBS}" "${EXPECTED_MAKE_JOBS}" make_jobs "Is the make job count correct with ${MESSAGE}"
    # shellcheck disable=SC2154
    _check_equal "${RPM_BUILD_NCPUS}" "${EXPECTED_RPM_BUILD_NCPUS}" rpm_build_ncpus "Is the RPM build CPU count correct with ${MESSAGE}"
)
check_cpu_count 1000 800000              ""    8    8    8 "a cgroup v1 limit"
check_cpu_count 1000  50000              ""    1    1    1 "a cgroup v1 limit for less than 1 cpu"
check_cpu_count    1 800000              ""    1    1    1 "a cgroup v1 limit higher than the number of cpus"
check_cpu_count 1000     -1              "" 1000 1000 1000 "an unbounded cgroup v1 limit"
check_cpu_count 1000     "" "800000 100000"    8    8    8 "a cgroup v2 limit"
check_cpu_count 1000     ""  "50000 100000"    1    1    1 "a cgroup v2 limit for less than 1 cpu"
check_cpu_count    1     "" "800000 100000"    1    1    1 "a cgroup v2 limit higher than the number of cpus"
check_cpu_count 1000     ""    "max 100000" 1000 1000 1000 "an unbounded cgroup v2 limit"
check_cpu_count 1000     ""              "" 1000 1000 1000 "no cgroup v1 or v2 limit file"

function check_join_by() {
    local RETURNED d
    local -a ARRAY=(a b c)
    local -A DELIMITER_EXPECTED=(["&"]="a&b&c" [-]="a-b-c" )
    for d in "${!DELIMITER_EXPECTED[@]}"; do
        RETURNED=$(join_by "${d}" "${ARRAY[@]}")
        _check_equal "${RETURNED}" "${DELIMITER_EXPECTED[${d}]}" "check_by returned" "Is the join_by value correct for ${d} ${ARRAY[*]}"
    done
}
check_join_by

function check_join_by_multi() {
    local RETURNED d
    local -a ARRAY=(a b c)
    local -A DELIMITER_EXPECTED
    DELIMITER_EXPECTED[" --foo "]=" --foo a --foo b --foo c"
    DELIMITER_EXPECTED[" --bar="]=" --bar=a --bar=b --bar=c"
    for d in "${!DELIMITER_EXPECTED[@]}"; do
        RETURNED=$(join_by_multi "${d}" "${ARRAY[@]}")
        _check_equal "${RETURNED}" "${DELIMITER_EXPECTED[${d}]}" "check_by returned" "Is the join_by_multi value correct for ${d} ${ARRAY[*]}"
    done
}
check_join_by_multi

function check_create_array_from_string() {
    local string1="foo bar"
    local string2="  foo bar"
    local string3="foo bar "
    local string4="foo|bar|baz"
    local string5="foo README*" returned expected
    local string1_array string2_array string3_array string4_array string5_array
    for i in string{1..5}; do create_array_from_string "${i}"; done

    returned="(${string1_array[*]})"
    expected="(foo bar)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string1}'"

    returned="(${string2_array[*]})"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string2}'"

    returned="(${string3_array[*]})"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string3}'"

    returned="(${string4_array[*]})"
    expected="(foo|bar|baz)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string4}'"

    returned="(${string5_array[*]})"
    expected="(foo README*)"
    _check_equal "${returned}" "${expected}" "check_by returned" "Is the create_array_from_string value correct for '${string5}'"
}
check_create_array_from_string

function check_remove_from_array() {
    local result expected i
    local -a array=()
    local -A input_expected
    input_expected["a b c"]="a c"
    input_expected["b a c"]="a c"
    input_expected["a c b"]="a c"
    input_expected["a b b b c"]="a c"
    input_expected["a c d"]="a c d"
    input_expected["b b b"]=""
    for i in "${!input_expected[@]}"; do
        expected=${input_expected[${i}]}
        read -r -a array <<< "${i}"
        remove_from_array array b
        result="(${array[*]+"${array[*]}"})"
         _check_equal "${result}" "(${expected})" "check_by returned" "Is the remove_from_array correct for (${i}) 'b'"
    done
    # It handles elements with spaces.
    array=("a b" "c d")
    remove_from_array array "a b"
    _check_equal "${#array[@]}" "1" "check_by returned" "Is the remove_from_array size correct for ('a b' 'c d') 'a b'"
    result="('${array[0]}')"
    _check_equal "${result}" "('c d')" "check_by returned" "Is the remove_from_array correct for ('a b' 'c d') 'a b'"
     # It doesn't fail when the input array is empty.
    array=()
    remove_from_array array b
    result="(${array[*]+"${array[*]}"})"
    _check_equal "${result}" "()" "check_by returned" "Is the remove_from_array correct for () 'b'"
}
check_remove_from_array

function check_is_true() {
    declare -A IS_TRUE_MAPPING
    IS_TRUE_MAPPING["true"]=0
    IS_TRUE_MAPPING["True"]=0
    IS_TRUE_MAPPING["false"]=1
    IS_TRUE_MAPPING["False"]=1
    IS_TRUE_MAPPING["randomvalue"]=1

    for VALUE in "${!IS_TRUE_MAPPING[@]}"; do
        is_true "${VALUE}" && RETURNED=0 || RETURNED=$?
        _check_equal "${RETURNED}" "${IS_TRUE_MAPPING[${VALUE}]}" "is_true returned" "Is the is_true value correct for ${VALUE}"
    done
}
check_is_true

function check_git_url() {
    _check_equal "$(git_clean_url "$1")" "$2" "git_clean_url" "Is the git_clean_url $3"
}
check_git_url "https://foo.bar/test.git/" "https://foo.bar/test.git/" "passing through correct URLs"
check_git_url "https://foo.bar/test.git"  "https://foo.bar/test.git/" "appending a slash"
check_git_url "https://foo.bar/test"      "https://foo.bar/test.git/" "appending .git/"
check_git_url "https://foo.bar/test/"     "https://foo.bar/test.git/" "appending .git with an existing slash"
check_git_url "foo.bar/test.git/"         "https://foo.bar/test.git/" "prepending the protocol"

function loop_helper {
    STATE="$(cat /tmp/loop_state)"
    echo "$((STATE + 1))" > /tmp/loop_state
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    echo loop${STATE}-start >> /tmp/loop_log
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    if (( STATE < $1 )); then
        false
    fi
    # shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
    echo loop${STATE}-end >> /tmp/loop_log
}

function check_loop_successful {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 3 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "3" "loops" "Is the command retried the correct number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "1" "completions" "Is the command completing exactly one     time"
    _check_equal "${s}" "0" "exit code" "Does the loop command exit correctly"
}
check_loop_successful

function check_loop_failed {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 10 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "5" "loops" "Is the command retried the maximum number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "0" "completions" "Is the command never completing"
    _check_equal "${s}" "1" "exit code" "Does the loop command fail correctly"
}
check_loop_failed

function check_dryrun_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/check_dryrun_by_upt.testfile

    local ARG_PY3=echo
    local ARG_KCIDB=${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}
    local ARG_UPT_INPUT=/tmp/input.xml
    local ARG_UPT_OUTPUT_YAML=/tmp/output.yaml
    local ARG_EXCLUDE_FILE=/tmp/hosts
    local ARG_UPT_OUTPUT_DIR=/tmp
    local EXCLUDE_ARGS=$1

    echo '{}' > "${ARG_KCIDB}"
    touch "${ARG_UPT_OUTPUT_YAML}"

    provision_and_test_by_upt "${ARG_PY3}" "${ARG_EXCLUDE_FILE}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_YAML}" "${ARG_UPT_OUTPUT_DIR}" "beaker" "high" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m upt \-\-time\-cap 4200 \-\-kcidb\-file ${ARG_KCIDB} \-\-max-aborted-count 6 ${EXCLUDE_ARGS}legacy convert \-i ${ARG_UPT_INPUT} \-r ${ARG_UPT_OUTPUT_YAML} \-p beaker \-\-priority high" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt legacy convert ok"

    CAPTURE=0
    grep "\-m upt.restraint.wrapper --help" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are dry run params passed to restraint runner ok"
}
skip_beaker=true check_dryrun_by_upt "-e /tmp/hosts "

function check_provision_and_test_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_PY3=echo
    local ARG_KCIDB=${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}
    local ARG_UPT_INPUT=/tmp/input.xml
    local ARG_UPT_OUTPUT_YAML=/tmp/output.yaml
    local ARG_EXCLUDE_FILE=/tmp/hosts
    local ARG_UPT_OUTPUT_DIR=/tmp
    local EXCLUDE_ARGS=$1

    echo '{}' > "${ARG_KCIDB}"
    touch "${ARG_UPT_OUTPUT_YAML}"

    provision_and_test_by_upt "${ARG_PY3}" "${ARG_EXCLUDE_FILE}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_YAML}" "${ARG_UPT_OUTPUT_DIR}" "beaker" "high" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m upt \-\-time\-cap 4200 \-\-kcidb\-file ${ARG_KCIDB} \-\-max-aborted-count 6 ${EXCLUDE_ARGS}legacy convert \-i ${ARG_UPT_INPUT} \-r ${ARG_UPT_OUTPUT_YAML} \-p beaker \-\-priority high" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt legacy convert ok"
    CAPTURE=0
    grep "\-m upt \-\-time\-cap 4200 \-\-kcidb\-file ${ARG_KCIDB} \-\-max-aborted-count 6 ${EXCLUDE_ARGS}provision --pipeline \-\-reruns 1 \-\-dump \-o ${ARG_UPT_OUTPUT_DIR} \-\-kcidb\-file ${ARG_KCIDB} \-r ${ARG_UPT_OUTPUT_YAML}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt provision ok"
}
check_provision_and_test_by_upt "-e /tmp/hosts "
hostname=host check_provision_and_test_by_upt ""

_failed_check
