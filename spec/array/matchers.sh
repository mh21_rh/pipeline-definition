# shellcheck shell=bash
# shellcheck disable=SC2317         # unreachable mocking code

shellspec_syntax 'shellspec_matcher_be_values'
function shellspec_matcher_be_values() {
    function shellspec_matcher__match() {
        local -i result=0 i=0
        SHELLSPEC_EXPECT=( "$@" )
        if [[ "${#SHELLSPEC_SUBJECT[@]}" != "${#SHELLSPEC_EXPECT[@]}" ]]; then
            result=1
        else
            for ((i = 0; i < ${#SHELLSPEC_EXPECT[@]}; ++i)); do
                if [[ "${SHELLSPEC_SUBJECT[${i}]}" != "${SHELLSPEC_EXPECT[${i}]}" ]]; then
                    result=1
                    break
                fi
            done
        fi
        # include all elements in messages
        SHELLSPEC_SUBJECT=${SHELLSPEC_SUBJECT[*]}
        # shellcheck disable=SC2178  # variable was used as an array before
        SHELLSPEC_EXPECT=${SHELLSPEC_EXPECT[*]}
        return "${result}"
    }
    # shellcheck disable=SC2016  # expressions don't expand in single quotes
    shellspec_syntax_failure_message + \
        'expected: $2' \
        '     got: $1'
    # shellcheck disable=SC2016  # expressions don't expand in single quotes
    shellspec_syntax_failure_message - \
        'expected: $2' \
        '     got: $1'

    shellspec_matcher_do_match "$@"
}

shellspec_syntax 'shellspec_matcher_be_empty'
function shellspec_matcher_be_empty() {
    function shellspec_matcher__match() {
        local -i result=0
        if [[ "${#SHELLSPEC_SUBJECT[@]}" != 0 ]]; then
            SHELLSPEC_SUBJECT="not empty"
            result=1
        else
            SHELLSPEC_SUBJECT="empty"
        fi
        return "${result}"
    }

    # shellcheck disable=SC2016  # expressions don't expand in single quotes
    shellspec_syntax_failure_message + \
        'expected: \"empty\"' \
        '     got: $1'
    # shellcheck disable=SC2016  # expressions don't expand in single quotes
    shellspec_syntax_failure_message - \
        'expected: \"not empty\"' \
        '     got: $1'

    shellspec_syntax_param count [ $# -eq 0 ] || return 1
    shellspec_matcher_do_match "$@"
}
